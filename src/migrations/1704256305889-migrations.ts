import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1704256305889 implements MigrationInterface {
    name = 'Migrations1704256305889'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "farmer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "firstname" character varying, "lastname" character varying, "email" character varying, "password" character varying, CONSTRAINT "PK_f356a29c5bf9598b298ea353c0e" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "farmer"`);
    }

}
