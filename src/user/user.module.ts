import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Farmer } from './user.entity';
import { BullModule } from '@nestjs/bull';
import { UserProcessor } from './user.processor';
import { NotificationProcessor } from './notification.processor';

@Module({
  imports: [
    TypeOrmModule.forFeature([Farmer]),
    BullModule.registerQueue({
      name : 'register-queue'
    },{
      name : 'notification-queue'
    })
  ],
  controllers: [UserController],
  providers: [UserService,UserProcessor,NotificationProcessor]
})
export class UserModule {}
