import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";

export class UserDto {
    @Expose()
    @ApiProperty({ required: false })
    public id: string;

    @Expose()
    @ApiProperty({ required: false })
    public firstname: string;

    @Expose()
    @ApiProperty({ required: false })
    public lastname: string;

    @Expose()
    @ApiProperty({ required: false })
    public email: string;

    @Expose()
    @ApiProperty({ required: false })
    public password: string;
}