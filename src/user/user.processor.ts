import { InjectQueue, Process, Processor } from "@nestjs/bull";
import { InjectRepository } from "@nestjs/typeorm";
import { Farmer } from "./user.entity";
import { Repository } from "typeorm";
import { Job, Queue } from "bull";

@Processor('register-queue')
export class UserProcessor{
    constructor(
        @InjectRepository(Farmer)
        private userRepository: Repository<Farmer>,
        @InjectQueue('register-queue') private userQueue: Queue,
        @InjectQueue('notification-queue') private notificationQueue: Queue,
    ){}

    @Process('register-job')
    async handleRegister(job: Job<any>){
        try{
            if(job.data.firstname === "dart"){
                throw {
                    id : job.id,
                    message : "บุคคลอันตราย"
                }
            }
            else{
                await this.userRepository.save(job.data)
                await this.notificationQueue.add({
                    message : `${job.data.email} is register successful`
                },{
                    removeOnComplete : true
                })
            }
        }
        catch(err){
            console.log(err)
        }
    }
}