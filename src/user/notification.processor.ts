import { InjectQueue, Process, Processor } from "@nestjs/bull";
import { Job, Queue } from "bull";

@Processor('notification-queue')
export class NotificationProcessor{
    constructor(
        @InjectQueue('notification-queue') private notificationQueue: Queue,
    ){}
    @Process()
    async handleNotification(job: Job<any>){
        console.log(job.data)
    }
}