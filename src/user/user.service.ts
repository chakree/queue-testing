import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Queue } from 'bull';
import { Farmer } from './user.entity';
import { Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';

@Injectable()
export class UserService {
    constructor(
      @InjectQueue('register-queue') private userQueue: Queue,
      @InjectRepository(Farmer)
        private userRepository: Repository<Farmer>
    ) {}
    
    async register(dto : UserDto){
        await this.userQueue.add('register-job',dto,{
            removeOnComplete : true,
        })
    }

    async registerWithOutQueue(dto : UserDto){
        await this.userRepository.save({
            firstname : dto.firstname,
            lastname : dto.lastname,
            email : dto.email,
            password : dto.password
        })
    }

    async getQueue(id : string){
        const client = await this.userQueue.client;
        const key = `bull:register-queue:${id}`;
        const res = await client.get(key)
        return res
    }
}
