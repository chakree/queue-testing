import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}
    @Post('register')
    async register(@Body() dto : UserDto) {
      await this.userService.register(dto);
      return { message: 'Registration in progress' };
    }

    @Post('register-without-queue')
    async registerWithOutQueue(@Body() dto : UserDto) {
        await this.userService.registerWithOutQueue(dto);
        return { message: 'Registration in progress without queue' };
      }

    @Get("/:id")
    async getQueue(@Param("id") id: string){
        return this.userService.getQueue(id)
    }
}
