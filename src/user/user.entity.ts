import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Farmer {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ name: 'firstname', nullable: true })
    firstname: string;

    @Column({ name: 'lastname', nullable: true })
    lastname: string;

    @Column({ name: 'email', nullable: true })
    email: string;

    @Column({ name: 'password', nullable: true })
    password: string;
}